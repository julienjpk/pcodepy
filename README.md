# pcodepy - a small and simple query tool for the postcodes.io API

pcodepy can be used to query basic information about a UK postcode from the 
command-line.

## Installation

pcodepy can be packaged and installed through its setuptools script:

```sh
$ python3 setup.py install --user
# or, as root:
$ python3 setup.py install
```

## Basic usage

Using pcodepy is pretty straightforward:

```sh
$ pcodepy [postcode]
```

## Alternatives

Note that you do not actually need to *install* pcodepy to run it. If you adjust
your `PYTHONPATH` accordingly, you may call the `__main__.py` script directly:

```sh
$ PYTHONPATH=. python3 pcodepy/__main__.py [postcode]
```

You may also import it in an IDE and simply use the entry point in 
`pcodepy/__main__.py`. **In that case, you will need to ensure your Python 
environment (ideally, a virtualenv) provides pcodepy's dependencies. You may do 
so using the following:**

```sh
$ pip install .
```

## Testing

If you wish to run the test suite before you install pcodepy, you may do so
using [tox](https://tox.readthedocs.io/en/latest/) at the root of the project:

```sh
$ tox
```

Since tox is not a runtime dependency, you will need to ensure it is available
in your Python environment:

```sh
$ pip install tox
```

## License

pcodepy is distributed under the terms of the MIT License.
