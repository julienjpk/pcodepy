#!/usr/bin/env python3
# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

from setuptools import setup, find_packages

setup(
    name='pcodepy',
    author='Julien JPK',
    author_email='julienjpk@email.com',
    version='1.0.0.dev1',
    license='MIT',
    description='A small and simple query tool for the postcodes.io API',
    keywords='python postcodes',
    url='https://gitlab.com/julienjpk/pcodepy',
    packages=find_packages(),
    install_requires=['requests'],
    entry_points={
        'console_scripts': [
            'pcodepy = pcodepy.__main__:main'
        ]
    }
)
