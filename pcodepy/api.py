# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

import requests


class NetworkError(Exception):
    """
    Raised whenever a network error occurs.
    """
    pass


class APIError(Exception):
    """
    Raised whenever the API provides an unexpected response.
    """
    pass


class InvalidPostcodeError(Exception):
    """
    Raised whenever a request is made about an invalid postcode
    """
    pass


class PostcodeInfo:
    """
    Transforms an info payload into an object, raising exceptions when needed
    """

    def __init__(self, payload):
        """
        Actually parses and validates the payload
        :param payload: The payload returned by postcodes.io for a postcode
        """
        try:
            self.postcode = payload['postcode']
            self.region = payload['region']
            self.country = payload['country']
        except KeyError:
            raise APIError("missing information in a postcode info payload")

    def __str__(self):
        """
        Pretty-printing helper for the main function.
        :return: A string which contains all the information about the postcode
        """
        return "Postcode: %s\n" \
               "Region:   %s\n" \
               "Country:  %s" % (self.postcode, self.region, self.country)


class Postcodes:
    """
    This class represents an HTTP session to the postcodes.io REST API.
    """

    # Base URL and important endpoints for pcodepy
    BASE_URL = 'https://api.postcodes.io/postcodes'
    INFO_URL = BASE_URL + '/%s'
    VALIDATE_URL = BASE_URL + '/%s/validate'
    NEAREST_URL = BASE_URL + '/%s/nearest'

    def __init__(self):
        """
        Creates the HTTP session object so we may take advantage of Keep-Alive.
        """
        self.http = requests.Session()

    def simple_json_get(self, url):
        """
        A simple helper function to perform a GET request and get a JSON payload
        :param url: The URL to query
        :return: The response payload received
        """
        try:
            response = self.http.get(url)
            if response.status_code != 200:
                raise APIError("unexpected status code: %d" %
                               response.status_code)

            try:
                return response.json()
            except ValueError:
                raise APIError("the API did not return a valid JSON payload")
        except requests.HTTPError:
            raise NetworkError("could not interpret an HTTP response")
        except requests.ConnectionError:
            raise NetworkError("could not connect to postcodes.io")
        except requests.Timeout:
            raise NetworkError("postcodes.io is taking too long to respond")
        except requests.RequestException:
            raise NetworkError("could not pinpoint the source of the error")

    def is_valid(self, postcode):
        """
        Verifies the validity of a postcode
        May raise APIError in case of an unexpected API update at postcodes.io
        :param postcode: The postcode to be checked
        :return: True if the postcode is valid, False otherwise
        """
        if len(postcode) <= 0:
            return False

        url = Postcodes.VALIDATE_URL % postcode

        try:
            response = self.simple_json_get(url)
            if 'result' not in response:
                raise APIError("no result key in the validation payload")
            return response['result']
        except ValueError:
            raise APIError("non-JSON response to a validation request")

    def info(self, postcode):
        """
        Fetches basic information about a postcode
        :param postcode: The postcode to look up
        :return: a PostcodeInfo object holding the requesting information
        """
        if not self.is_valid(postcode):
            raise InvalidPostcodeError()

        url = Postcodes.INFO_URL % postcode

        try:
            response = self.simple_json_get(url)
            if 'result' not in response:
                raise APIError("no result key in the info payload")
            return PostcodeInfo(response['result'])
        except ValueError:
            raise APIError("non-JSON response to an info request")

    def nearest(self, postcode):
        """
        Fetches the list of all postcodes nearest to a specific one
        :param postcode: The postcode to centre around
        :return: A list of PostcodeInfo objects
        """
        if not self.is_valid(postcode):
            raise InvalidPostcodeError()

        url = Postcodes.NEAREST_URL % postcode

        try:
            response = self.simple_json_get(url)
            if 'result' not in response:
                raise APIError("no result key in the nearest payload")
            return [PostcodeInfo(p) for p in response['result']]
        except ValueError:
            raise APIError("non-JSON response to a nearest request")
