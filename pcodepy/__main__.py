# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

import pcodepy.api as api

import sys


def main():
    if len(sys.argv) < 2:
        print("Usage: %s [postcode]" % sys.argv[0], file=sys.stderr)
        return 1

    # Postcodes may contain spaces, and users sometimes forget quotes.
    postcode = ' '.join(sys.argv[1:])

    # Get ready to talk to postcodes.io
    client = api.Postcodes()

    # Get information about the provided postcode and handle errors
    try:
        info = client.info(postcode)
        nearest = client.nearest(postcode)
    except api.InvalidPostcodeError:
        print("Invalid postcode: %s" % postcode, file=sys.stderr)
        return 2
    except api.APIError as e:
        print("postcodes.io returned an unexpected response (%s)" % str(e),
              file=sys.stderr)
        return 3
    except api.NetworkError as e:
        print("A network error occurred (%s)" % str(e), file=sys.stderr)
        return 4

    # Print the information and return!
    print("%s\n\nNearest postcodes:\n\n%s" % (
        info,
        '\n\n'.join(str(i) for i in nearest)
    ))

    return 0


if __name__ == '__main__':
    sys.exit(main())
