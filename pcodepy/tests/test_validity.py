# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

from pcodepy.tests.fixtures import client

import pcodepy.api as api


def test_validity_ok(client: api.Postcodes):
    assert client.is_valid('CB3 0FA')


def test_validity_no_spaces_ok(client: api.Postcodes):
    assert client.is_valid('CB30FA')


def test_validity_ko(client: api.Postcodes):
    assert not client.is_valid('000 000')


def test_validity_empty_ko(client: api.Postcodes):
    assert not client.is_valid('')
