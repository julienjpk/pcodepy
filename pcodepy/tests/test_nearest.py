# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

from pcodepy.tests.fixtures import client

import pcodepy.api as api

import pytest


def test_nearest_ok(client: api.Postcodes):
    nearest = client.nearest('CB3 0FA')
    assert len(nearest) > 0


def test_nearest_invalid_ko(client: api.Postcodes):
    with pytest.raises(api.InvalidPostcodeError):
        client.nearest('')
