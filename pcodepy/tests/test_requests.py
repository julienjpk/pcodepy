# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

from pcodepy.tests.fixtures import client

import pcodepy.api as api

import pytest


def test_valid_request(client: api.Postcodes):
    url = api.Postcodes.INFO_URL % 'CB3 0FA'
    payload = client.simple_json_get(url)
    assert isinstance(payload, dict)


def test_invalid_request(client: api.Postcodes):
    url = api.Postcodes.BASE_URL + '/this/should/never/exist'
    with pytest.raises(api.APIError):
        client.simple_json_get(url)
