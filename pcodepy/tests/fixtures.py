# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

import pcodepy.api as api

import pytest


@pytest.fixture()
def client():
    return api.Postcodes()


@pytest.fixture()
def pinfo():
    return api.PostcodeInfo({
        'postcode': 'CB3 0FA',
        'region': 'East of England',
        'country': 'England'
    })
