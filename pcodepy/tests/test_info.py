# pcodepy - Copyright (c) 2020 Julien JPK
# pcodepy is distributed under the terms of the MIT License

from pcodepy.tests.fixtures import client, pinfo

import pcodepy.api as api

import pytest


def test_pinfo_str_ok(pinfo: api.PostcodeInfo):
    infostr = str(pinfo)
    assert pinfo.postcode in infostr
    assert pinfo.region in infostr
    assert pinfo.country in infostr


def test_pinfo_no_postcode_ko():
    with pytest.raises(api.APIError):
        api.PostcodeInfo({'region': None, 'country': None})


def test_pinfo_no_region_ko():
    with pytest.raises(api.APIError):
        api.PostcodeInfo({'postcode': None, 'country': None})


def test_pinfo_no_country_ko():
    with pytest.raises(api.APIError):
        api.PostcodeInfo({'postcode': None, 'region': None})


def test_info_valid_ok(client: api.Postcodes, pinfo: api.PostcodeInfo):
    info = client.info(pinfo.postcode)
    assert isinstance(info, api.PostcodeInfo)
    assert info.postcode == pinfo.postcode
    assert info.region == pinfo.region
    assert info.country == pinfo.country


def test_info_invalid_ko(client: api.Postcodes):
    with pytest.raises(api.InvalidPostcodeError):
        client.info('')
